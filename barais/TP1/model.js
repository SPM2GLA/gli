
// Implémenter ici les 4 classes du modèle.
// N'oubliez pas l'héritage !

function Drawing(){
var formes = array();
}

function Forme(positioninitialX, positioninitialY, positionfinaleX, positionfinaleY, epaisseur, couleur){
// var couleur = '#000000';
// var epaisseur = 2;
// var positioninitialX = 0;
// var positioninitialY = 0;
// var positionfinaleX = 0;
// var positionfinaleY = 0;
this.couleur = couleur;
this.epaisseur = epaisseur;
this.positioninitialX=positioninitialX;
this.positioninitialY=positioninitialY;
this.positionfinaleX=positionfinaleX; // ou largeur pour Rectangle
this.positionfinaleY=positionfinaleY; // ou hauteur pour Rectangle

this.getInitX = function() {
    return   this.positioninitialX;
  }.bind(this) ;

  this.getInitY = function() {
    return   this.positioninitialY;

  }.bind(this) ;
  this.getFinalX = function() {
    return   this.positionfinaleX;

  }.bind(this) ;
  this.getFinalY = function() {
    return   this.positionfinaleY;

  }.bind(this) ;

  this.getEpaisseur = function() {
    return   this.epaisseur;

  }.bind(this) ;

  this.getCouleur = function() {
    return   this.couleur;
}.bind(this) ;

}

function Rectangle(positioninitialX, positioninitialY, positionfinaleX, positionfinaleY, epaisseur, couleur){
  Forme.call(this,positioninitialX, positioninitialY, positionfinaleX, positionfinaleY, epaisseur, couleur);
};
Rectangle.prototype = new Forme(); // définition de l'héritage

function Ligne(positioninitialX, positioninitialY, positionfinaleX, positionfinaleY, epaisseur, couleur){
  Forme.call(this,positioninitialX, positioninitialY, positionfinaleX, positionfinaleY, epaisseur, couleur);
};
Ligne.prototype = new Forme();
