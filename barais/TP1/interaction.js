
// La création d'un Dnd requière un canvas et un interacteur.
// L'interacteur viendra dans un second temps donc ne vous en souciez pas au départ.
function DnD(canvas, interactor) {
	// Définir ici les attributs de la 'classe'
  var click=false;
  var positioninitialX=0;
  var positioninitialY=0;
  var positionfinaleX=0;
  var positionfinaleY=0;
  this.canvas=canvas;
  this.interactor=interactor;//recup pour le pencil
	// Developper les 3 fonctions gérant les événements
  this.fonction1=function(evt){ // pression souris
    console.log(getMousePosition(this.canvas,evt));
    this.click=true;
    this.positioninitialX=getMousePosition(this.canvas,evt).x;
    this.positioninitialY=getMousePosition(this.canvas,evt).y;
    this.interactor.onInteractionStart(this);
  }.bind(this);
  this.fonction2 = function(evt){ //déplacement souris

    if (this.click == true){
      console.log(getMousePosition(this.canvas,evt));
      this.positionfinaleX=getMousePosition(this.canvas,evt).x;
      this.positionfinaleY=getMousePosition(this.canvas,evt).y;
      this.interactor.onInteractionUpdate(this);
    }
  }.bind(this);
  this.fonction3=function(evt){ // relachement souris
    console.log(getMousePosition(this.canvas,evt));
    console.log(evt);
    console.log(canvas);
    this.click=false;
    this.positionfinaleX=getMousePosition(this.canvas,evt).x;
    this.positionfinaleY=getMousePosition(this.canvas,evt).y;

    this.interactor.onInteractionEnd(this);
  }.bind(this);
	// Associer les fonctions précédentes aux évènements du canvas.

  canvas.addEventListener('mousedown', this.fonction1, false);
  canvas.addEventListener('mousemove', this.fonction2, false);
  canvas.addEventListener('mouseup', this.fonction3, false);

};


var canvas = document.getElementById("mycanvas");
// Place le point de l'événement evt relativement à la position du canvas.
function getMousePosition(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX -rect.left,
    y: evt.clientY -rect.top
    //positionfinaleX,//positionfinaleY
  };
};
