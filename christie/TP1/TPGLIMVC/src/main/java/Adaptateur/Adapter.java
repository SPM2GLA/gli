package Adaptateur;



import java.util.List;

import Model.IModel;
import Model.Item;
import Vue.IView;

public class Adapter implements IModel {
	private IView view;
	private IModel model;
	

	public IView getView() {
		return view;
	}

	public void setView(IView view) {
		this.view = view;
	}

	public IModel getModel() {
		return model;
	}

	public void setModel(IModel model) {
		this.model = model;
	}
	
	public void updateView(){
		model.updateView();
		view.updateView();
	}

	public void addItem(Item i) {
		// TODO Auto-generated method stub
		
	}

	public void removeItem(Item i) {
		// TODO Auto-generated method stub
		
	}


	public List<Item> getItem() {
		// TODO Auto-generated method stub
		return model.getItem();
	}


}
