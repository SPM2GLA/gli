package Vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Adaptateur.Adapter;
import Controller.Controller;
import Controller.IController;
import Model.IModel;
import Model.Item;
import Model.Model;

public class MonComposant {

	public static void main(String[] args) {
		Model model = new Model();
		View view = new View();
		Adapter adapter = new Adapter();
		Controller controller = new Controller();
		view.setModel(adapter);
		controller.setModel(adapter);
		controller.setView(view);
		adapter.setModel(model);
		adapter.setView(view);

		model.setName("toto");
		model.addItem(new Item("impots", "impots de \nma maison", 1024));
		model.addItem(new Item("Voiture", "assurance de \nma voiture", 42));
		model.addItem(new Item("electricite", "pratique", 512));
		model.addItem(new Item("gaz", "trop chaud", 128));
		model.addItem(new Item("chat", "meow !", 48));
		model.addItem(new Item("loyer", "trop cher", 2048));
		model.addItem(new Item("lapin", "squick !", 47));
		model.addItem(new Item("chien", "poop !", 46));
		model.addItem(new Item("jardinier", "trop vert", 666));
		model.addItem(new Item("charges", "utile ?", 32));

		JFrame f = new JFrame("Camembert");
		JFrame f2 = new JFrame("Jtable");
		// la Jtable
		JTable table = new JTable(new TableModelJtalbe(adapter));
		JScrollPane scrollPane = new JScrollPane(table);
		table.setFillsViewportHeight(true);
//		 f.setBounds(30, 30, 700, 700);

		// f.getContentPane().add(scrollPane);
		// f.pack();
		SharedListSelectionHandler listener = new SharedListSelectionHandler();
		listener.setView(view);
		table.getSelectionModel().addListSelectionListener(listener);
		table.getColumnModel().getSelectionModel().addListSelectionListener(listener);

		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//		f.setPreferredSize(new Dimension(700, 700));
		 f.setBounds(30, 30, 400, 200);
		// f.getContentPane().add(view);
//		 f.pack();
		f.setVisible(true);
		view.creatView();
		// v.paintComponent();
		f.addMouseListener(controller);

		view.setJtable(table);
		JPanel p = new JPanel();
		p.setPreferredSize(new Dimension(600, 600));
		JPanel p2 = new JPanel();
		p2.setPreferredSize(new Dimension(400, 600));
		scrollPane.setPreferredSize(new Dimension(400, 600));
		table.setPreferredSize(new Dimension(400, 600));
		f.setLayout(new FlowLayout());
		p.add(view);
		f.getContentPane().add(p);
		p2.add(scrollPane);
		f.getContentPane().add(p2);
		f.pack();
//		f2.add(scrollPane);
//		f.add(table);
//		f.add(view);

	}

}