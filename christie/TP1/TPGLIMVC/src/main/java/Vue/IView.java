package Vue;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.util.List;

import javax.swing.JTable;

public interface IView {

	void updateView();
	public List<Composant> getListeComposant();
	public void setSelectedComposant(int index);
	public int getSelectedComposant();
	public Rectangle2D.Float getB_next();
	public Rectangle2D.Float getB_previous();
	public JTable getJtable();
	public void setJtable(JTable jtable);
}
