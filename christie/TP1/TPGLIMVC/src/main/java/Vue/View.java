package Vue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Model.IModel;

public class View extends JComponent implements IView {

	Graphics2D g2d;	
	IModel model;
	List<Composant> listeComposant = new ArrayList<Composant>();
	int selectedComposant = -1;
	Rectangle2D.Float b_next;
	Rectangle2D.Float b_previous;
	JTable jtable;
	public JTable getJtable() {
		return jtable;
	}

	public void setJtable(JTable jtable) {
		this.jtable = jtable;
	}


	//position & taille
	int posX = 250;
	int posY = 250;
	int sizeX = 200;
	int sizeY = 200;
	public IModel getModel() {
		return model;
	}

	public void setModel(IModel model) {
		this.model = model;
	}

	public void creatView(){

	}

	public void paintComponent(Graphics g) {

		listeComposant.clear();
		super.paintComponent(g);
		g2d = (Graphics2D) g;

		double total=0;
		for (int i = 0; i < model.getItem().size(); i++) {
			total += model.getItem().get(i).getValeur();
		}
		double previousAngle=0;
		for (int i = 0; i < model.getItem().size(); i++) {
			//dessin des labels
			double distance = 250+(-i*40)%120;
			double angle = (previousAngle+180*model.getItem().get(i).getValeur()/total)*0.01745;
			double x=posX+(distance*Math.cos(angle));
			double y=posY-(distance*Math.sin(angle));


			//dessin ligne
			Line2D.Float l = new Line2D.Float((float)x, (float)y, posX, posY);
			g2d.setColor(Color.BLACK);
			g2d.draw(l);

			//dessin des cercles
			Arc2D.Float arc=new Arc2D.Float(Arc2D.PIE);
			arc.setFrame(posX-sizeX/2,posY-sizeY/2,sizeX,sizeY);
			if(selectedComposant != -1 && i ==selectedComposant)
				arc.setFrame(posX-25-sizeX/2,posY-25-sizeY/2,sizeX+50,sizeY+50);
			arc.setAngleStart(previousAngle);
			arc.setAngleExtent(360*model.getItem().get(i).getValeur()/total);

			switch (i%6) {
			case 0:
				g2d.setColor(Color.BLACK);
				break;
			case 1:
				g2d.setColor(Color.GRAY);
				break;
			case 2:
				g2d.setColor(Color.RED);
				break;
			case 3:
				g2d.setColor(Color.GREEN);
				break;
			case 4:
				g2d.setColor(Color.ORANGE);
				break;
			case 5:
				g2d.setColor(Color.PINK);
				break;
			default:
				g2d.setColor(Color.YELLOW);
				break;
			}
			if(selectedComposant != -1 && i ==selectedComposant)
				g2d.setColor(Color.BLUE);
			g2d.fill(arc);
			g2d.setColor(Color.WHITE);
			g2d.draw(arc);
			//dessin box
			Rectangle2D.Float r = new Rectangle2D.Float((float)x-10, (float)y-15, 80, 20);
			if(selectedComposant != -1 && i ==selectedComposant)
				r.setRect((float)x-10, (float)y-15, 80, 60);

			g2d.setColor(Color.WHITE);
			g2d.fill(r);
			g2d.setColor(Color.BLACK);
			g2d.draw(r);
			g2d.setColor(Color.BLUE);
			g2d.drawString( model.getItem().get(i).getTitre() , (int)x, (int)y);
			if(selectedComposant != -1 && i ==selectedComposant){
				g2d.drawString( model.getItem().get(i).getDescription() , (int)x+1, (int)y+13);
				g2d.drawString( "£"+model.getItem().get(i).getValeur() , (int)x+1, (int)y+39);
			}
			listeComposant.add(new Composant(r, arc, model.getItem().get(i)));
			previousAngle += 360*model.getItem().get(i).getValeur()/total;
			
			// rond milieu
			arc=new Arc2D.Float(Arc2D.PIE);
			int nsize = 150;
			arc.setFrame(posX-nsize/2,posY-nsize/2,nsize,nsize);
			arc.setAngleStart(0);
			arc.setAngleExtent(360);
			g2d.setColor(Color.WHITE);
			g2d.draw(arc);
			g2d.fill(arc);
			nsize = 120;
			arc.setFrame(posX-nsize/2,posY-nsize/2,nsize,nsize);
			g2d.setColor(Color.BLACK);
			g2d.draw(arc);
			g2d.fill(arc);
			g2d.setColor(Color.WHITE);
			g2d.drawString( "Total" , posX-15, posY-20);
			g2d.drawString( ""+total+"€" , posX-15, posY);
		

			g2d.setColor(Color.BLACK);
			g2d.drawString( "bug détection click sur les rectangle, il y a un décallage, c'est java." , 10, 550);
			g2d.drawString( "!!! NEW PAS METTRE EN PLEIN ECRAN !!!" , 10, 580);
		}



		g2d.setFont(new Font("Arial", Font.BOLD, 14));
		g2d.setColor(Color.WHITE);

		// TODO: utilisation des données du IModel pour l'affichage

		//dessin next & previous
		if(selectedComposant != -1 && b_next == null){
			Rectangle2D.Float r = new Rectangle2D.Float(0, 0, 100, 20);
			g2d.setColor(Color.WHITE);
			g2d.fill(r);
			g2d.setColor(Color.BLACK);
			g2d.draw(r);
			g2d.setColor(Color.BLUE);
			g2d.drawString( "NEXT" , 10, 15);
			b_next =r;
			Rectangle2D.Float r2 = new Rectangle2D.Float(0, 20, 100, 20);
			g2d.setColor(Color.WHITE);
			g2d.fill(r2);
			g2d.setColor(Color.BLACK);
			g2d.draw(r2);
			g2d.setColor(Color.BLUE);
			g2d.drawString( "PREVIOUS" , 10, 35);
			b_previous = r2;
		}
		else if (selectedComposant >= 0 && b_next!=null) {
			g2d.setColor(Color.WHITE);
			g2d.fill(b_next);
			g2d.setColor(Color.BLACK);
			g2d.draw(b_next);
			g2d.setColor(Color.BLUE);
			g2d.drawString( "NEXT" , 10, 15);
			g2d.setColor(Color.WHITE);
			g2d.fill(b_previous);
			g2d.setColor(Color.BLACK);
			g2d.draw(b_previous);
			g2d.setColor(Color.BLUE);
			g2d.drawString( "PREVIOUS" , 10, 35);
		}
	
}




public Rectangle2D.Float getB_next() {
	return b_next;
}

public Rectangle2D.Float getB_previous() {
	return b_previous;
}

public List<Composant> getListeComposant() {
	return listeComposant;
}


public void updateView() {
	// TODO Auto-generated method stub
	repaint();
	//recharger les données du model
}


public void setSelectedComposant(int index) {
	this.selectedComposant = index;
}

public int getSelectedComposant() {
	return selectedComposant;
}
@Override
public Dimension getPreferredSize() {
        return new Dimension(600, 600);
}
}
