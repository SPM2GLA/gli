package Vue;

import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Float;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

import Model.Item;

public class Composant {
	Rectangle2D.Float Rectangle;
	Arc2D.Float MorceauCamenbert;
	Item item;
	
	public Rectangle2D.Float getRectangle() {
		return Rectangle;
	}
	public Arc2D.Float getMorceauCamenbert() {
		return MorceauCamenbert;
	}
	public Item getItem() {
		return item;
	}
	public Composant(Float rectangle, java.awt.geom.Arc2D.Float morceauCamenbert, Item item) {
		super();
		Rectangle = rectangle;
		MorceauCamenbert = morceauCamenbert;
		this.item = item;
	}
	public Composant() {
	}
	
}
