package Vue;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import Model.IModel;


public class TableModelJtalbe extends AbstractTableModel implements ListSelectionModel{

	IModel model;

	public int getColumnCount() {
		return 3;
	}

	public TableModelJtalbe(IModel model) {
		super();
		this.model = model;
	}

	public String getColumnName(int col) {

		
		
		switch (col) {
		case 0:
			return "titre";
		case 1:
			return "description";
		case 2:
			return "valeur";

		default:
			break;
		}
		return null;
	}
	public int getRowCount() {
		
		return model.getItem().size();
	}

	public Object getValueAt(int row, int col) { //row collum
		switch (col) {
		case 0:
			return model.getItem().get(row).getTitre();	
		case 1:
			return model.getItem().get(row).getDescription();	
		case 2:
			return model.getItem().get(row).getValeur();	
		default:
			break;
		}

		return null;
	}

	public void setValueAt(Object value, int row, int col) {
		switch (col) {
		case 0:
			model.getItem().get(row).setTitre((String) value);
			break;
		case 1:
			model.getItem().get(row).setDescription((String) value);
			break;
		case 2:
			model.getItem().get(row).setValeur(Integer.parseInt((String) value));
			break;
		default:
			break;
		}
		fireTableCellUpdated(row, col);
		model.updateView();
	}
	public boolean isCellEditable(int row, int col) {
		//Note that the data/cell address is constant,
		//no matter where the cell appears onscreen.
		return true;
	}

	public void addListSelectionListener(ListSelectionListener x) {
		// TODO Auto-generated method stub
		
	}

	public void addSelectionInterval(int index0, int index1) {
		// TODO Auto-generated method stub
		
	}

	public void clearSelection() {
		// TODO Auto-generated method stub
		
	}

	public int getAnchorSelectionIndex() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getLeadSelectionIndex() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getMaxSelectionIndex() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getMinSelectionIndex() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getSelectionMode() {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean getValueIsAdjusting() {
		// TODO Auto-generated method stub
		return false;
	}

	public void insertIndexInterval(int index, int length, boolean before) {
		// TODO Auto-generated method stub
		
	}

	public boolean isSelectedIndex(int index) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isSelectionEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	public void removeIndexInterval(int index0, int index1) {
		// TODO Auto-generated method stub
		
	}

	public void removeListSelectionListener(ListSelectionListener x) {
		// TODO Auto-generated method stub
		
	}

	public void removeSelectionInterval(int index0, int index1) {
		// TODO Auto-generated method stub
		
	}

	public void setAnchorSelectionIndex(int index) {
		// TODO Auto-generated method stub
		
	}

	public void setLeadSelectionIndex(int index) {
		// TODO Auto-generated method stub
		
	}

	public void setSelectionInterval(int index0, int index1) {
		// TODO Auto-generated method stub
		
	}

	public void setSelectionMode(int selectionMode) {
		// TODO Auto-generated method stub
		
	}

	public void setValueIsAdjusting(boolean valueIsAdjusting) {
		// TODO Auto-generated method stub
		
	}

	
}

