package Model;

import java.util.ArrayList;
import java.util.List;



public class Model implements IModel{
	private String name;
	private List<Item> liste = new ArrayList<Item>();

	public Model(String name, List<Item> liste) {
		super();
		this.name = name;
		this.liste = liste;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Item> getListe() {
		return liste;
	}
	public Model() {
		super();
		// TODO Auto-generated constructor stub
	}
	public void setListe(List<Item> liste) {
		this.liste = liste;
	}
	
	public void addItem(Item i){
		
		liste.add(i);
	}
		public void removeItem(Item i){
			
			liste.remove(i);
		}
		public void updateView() {
			// TODO Auto-generated method stub
			
		}
		public List<Item> getItem() {
			// TODO Auto-generated method stub
			return liste;
		}
		
		
}
