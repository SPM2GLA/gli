package Model;

import java.util.List;

public interface IModel {
	

	public List<Item> getItem();
	public void addItem(Item i);
	public void removeItem(Item i);
	public void updateView();

}
