package Model;

public class Item {
private String titre;
private String description;
private int valeur;
public Item(String titre, String description, int valeur) {
	super();
	this.titre = titre;
	this.description = description;
	this.valeur = valeur;
}
public Item() {
	super();
}
public String getTitre() {
	return titre;
}
public void setTitre(String titre) {
	this.titre = titre;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public int getValeur() {
	return valeur;
}
public void setValeur(int valeur) {
	this.valeur = valeur;
}


}
