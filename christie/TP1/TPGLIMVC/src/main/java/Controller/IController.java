package Controller;

import java.awt.event.MouseListener;

public interface IController extends MouseListener{
	public void updateView();
	public void updateJModelView();
}
