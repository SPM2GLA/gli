package Controller;

import java.awt.event.MouseEvent;

import Model.IModel;
import Vue.IView;

public class Controller implements IController{

	private int setSelectedComposant;
	
	IModel model;
	IView view;
	
	public IModel getModel() {
		return model;
	}
	public void setModel(IModel model) {
		this.model = model;
	}
	public IView getView() {
		return view;
	}
	public void setView(IView view) {
		this.view = view;
	}
	public Controller() {
		super();
		// TODO Auto-generated constructor stub
	}
	public void updateView(){
		model.updateView();
	}
	
	public void updateJModelView() {
		view.getJtable().setRowSelectionInterval(view.getSelectedComposant(), view.getSelectedComposant());
		
	}
	
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		System.err.println("click !");
		for (int i = 0; i < view.getListeComposant().size(); i++) {
			if (view.getListeComposant().get(i).getMorceauCamenbert().contains(e.getX(), e.getY()) | view.getListeComposant().get(i).getRectangle().contains(e.getX(),e.getY())){
				view.setSelectedComposant(i);
				setSelectedComposant=i;
				view.updateView();
				updateJModelView();
				break;
			}
			
		}
	}
	
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		if (view.getB_next() != null && view.getB_next().contains(e.getX(), e.getY())) {
			setSelectedComposant = ++setSelectedComposant%view.getListeComposant().size();
			view.setSelectedComposant(setSelectedComposant);
			
			view.updateView();
			updateJModelView();
		}
		else if (view.getB_previous() != null && view.getB_previous().contains(e.getX(), e.getY())) {
			setSelectedComposant = --setSelectedComposant%view.getListeComposant().size();
			if (setSelectedComposant<0)setSelectedComposant=view.getListeComposant().size()-1;
			view.setSelectedComposant(setSelectedComposant);
			view.updateView();
			updateJModelView();
		}
	}
	
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
